[Link to project slide](https://docs.google.com/presentation/d/1veA_WQcfRRx7hQnE8qklmsLYceWzQGrPHrieSEWqcaI/edit#slide=id.g2a549964dd_8_13)

[Link to analyse doc](https://docs.google.com/document/d/1Icl5gMxiXM7gqzGfBhBlEJuPIW-K7KlFEs2RIjmza9s/edit#heading=h.80gzrc8yz49d)

[Link to Kaggle kernel](https://www.kaggle.com/marekpagel/video-game-sales-throughout-1985-2017)

 * exploration.R was used to explore the initial data.
 * cleaning.R file was used to clean the data.
 * merging_of_video_game_datasets.R was used to merge the two video game datasets (IGN and Metacritic).
 * correlation.R file was used to test for correlations between scores and sales.
 * graphs.R file was used to create the necessary graphs.
